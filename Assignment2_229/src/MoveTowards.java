import bos.GamePiece;
import bos.MoveDown;
import bos.MoveLeft;
import bos.MoveRight;
import bos.MoveUp;
import bos.NoMove;
import bos.RelativeMove;

import java.util.List;
import java.util.Queue;

public class MoveTowards implements Behaviour {
    Character target;

    public MoveTowards(Character character){
        this.target = character;
    }

    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        List<RelativeMove> movesToTarget = stage.grid.movesBetween(mover.location,target.location, mover);


        mover.setTargetOf(target.location);
        if (movesToTarget.size() == 0) {
            return new NoMove(stage.grid, mover);
        }
        else
        {

            return movesToTarget.get(0);
        }}


}




