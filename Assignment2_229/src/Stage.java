import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.GameBoard;
import bos.Rabbit;
import bos.RelativeMove;

public class Stage extends KeyObservable { // implements KeyObserver

    private static Stage uniqueInstance;

    protected static Grid grid;
    protected static Character sheep;
    protected static Character shepherd;
    protected static Character wolf;
    // protected static Character rabbit;
    private List<Character> allCharacters;
    protected static Player player;
    protected careTaker caretaker;

    private Instant timeOfLastMove = Instant.now();

    private Stage() {
        grid = Grid.getInstance();
        SAWReader sr = new SAWReader("data/stage1.saw");
        shepherd = new Shepherd(grid.cellAtRowCol(sr.getShepherdLoc().first, sr.getShepherdLoc().second), new StandStill());
        sheep    = new Sheep(grid.cellAtRowCol(sr.getSheepLoc().first, sr.getSheepLoc().second), new SmartSearch(shepherd)); //new MoveTowards(shepherd));
        wolf     = new Wolf(grid.cellAtRowCol(sr.getWolfLoc().first, sr.getWolfLoc().second), new SmartSearch(sheep));
        player = new Player(grid.getRandomCell());
        this.register(player);
        caretaker = new careTaker();
        this.register(caretaker);
        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep); allCharacters.add(shepherd); allCharacters.add(wolf);

        sr.searchForBlocked();

        for (bos.Pair<Integer, Integer> pair : sr.searchForBlocked()) {
            grid.cellAtRowCol(pair.first, pair.second).blocked = true;

        }
        sr.searchForWarp();

        for (bos.Pair<Integer, Integer> pair : sr.searchForWarp()) {
            grid.cellAtRowCol(pair.first, pair.second).warps = true;
        }

        // bunch of initialisers for the characters to troubleshoot and test, comment out the initialisation above and comment in what you need!

        //rabbit = new rabbitAdapter(grid.cellAtRowCol(2, 2), null);
        // shepherd = new Shepherd(grid.cellAtRowCol(7, 10), new StandStill());
        //  sheep    = new Sheep(grid.cellAtRowCol(sr.getSheepLoc().first, sr.getSheepLoc().second), new StandStill()); //new MoveTowards(shepherd));
        //sheep    = new Sheep(grid.cellAtRowCol(10,5), new StandStill());
        //wolf = new Wolf(grid.cellAtRowCol(19,19), new MoveTowards(sheep));
        //  wolf     = new Wolf(grid.cellAtRowCol(sr.getWolfLoc().first, sr.getWolfLoc().second), new StandStill());
        //player.setLocationOf(grid.cellAtRowCol(4, 4));
        // this.register(this);
        //allCharacters.add(rabbit);



    }

    public static Stage getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new Stage();
        }
        return uniqueInstance;
    }

    public void update(){

        if (!player.inMove()) {

            for (Character character: allCharacters) {
                if (grid.cellAtRowCol((int) character.location.x/35, (int) character.location.y/35).warps == true) {
                    character.setLocationOf(grid.getRandomCell());
                }
            }
            if (grid.cellAtRowCol(player.location.x/35, player.location.y/35).warps == true) {
                player.setLocationOf(grid.getRandomCell());
            }

            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                if (sheep.location.x != sheep.location.y) {
                    sheep.setBehaviour(new SmartSearch(shepherd));
                    shepherd.setBehaviour(new StandStill());
                    sheep.setTargetOf(null);
                }
                if (sheep.location.x == sheep.location.y) {
                    sheep.setBehaviour(new StandStill());
                    shepherd.setBehaviour(new SmartSearch(sheep));
                    shepherd.setTargetOf(null);
                }
                allCharacters.forEach((c) -> c.aiMove(this).perform());

                player.startMove();
                timeOfLastMove = Instant.now();
            }
        }

    }

    public void paint(Graphics g, Point mouseLocation) {

        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);

        //create the path from the character to its location :)
        for (Character character: allCharacters ) {
            if (character.getTargetOf() != null && character.getLocationOf() != character.getTargetOf()) {

                List<Cell> path = breadthFirstSearch.breadthSearch(character.getLocationOf(), character.getTargetOf());


                g.setColor(Color.GRAY);

                for(Cell paths: path) {
                    if (mouseLocation != null) {
                        Point pos = mouseLocation;

                        if (pos.x >= character.getLocationOf().x && pos.x <= character.getLocationOf().x+35 && pos.y >= character.getLocationOf().y && pos.y <= character.getLocationOf().y+35)
                            g.fillRect( paths.x+10, paths.y+10, 15, 15);

                    }
                }

            }else if (character.getTargetOf() == null) {} }
    }

    public void createSavePoint() {

        caretaker.saveMemento();
    }

    public void restoreSavePoint() {

        Memento mem = caretaker.restoreMemento();
        wolf.setLocationOf(mem.getWolfLocation());
        sheep.setLocationOf(mem.getSheepLocation());
        shepherd.setLocationOf(mem.getShepLocation());
        player.location = mem.getPlayerLocation();

    }
}



//}


