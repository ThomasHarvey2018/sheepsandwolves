import java.awt.Color;
import java.util.Optional;

import bos.Rabbit;
import bos.RelativeMove;

public class rabbitAdapter extends Character {

    private Rabbit rabbit;

    public rabbitAdapter(Cell location, Behaviour behaviour) {
        super(location, behaviour);
        this.rabbit = new Rabbit();
        behaviour = null;
        this.display = Optional.of(Color.GRAY);
    }

    public RelativeMove aiMove(Stage stage){
        if (rabbit.nextMove() == 0)
            return new bos.MoveLeft(stage.grid, this);
        if (rabbit.nextMove() == 1)
            return new bos.MoveRight(stage.grid, this);
        if (rabbit.nextMove() == 2)
            return new bos.MoveUp(stage.grid, this);
        if (rabbit.nextMove() == 3)
            return new bos.MoveDown(stage.grid, this);
        return null;
    }


}
