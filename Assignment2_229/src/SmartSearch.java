import java.util.List;

import bos.NoMove;
import bos.RelativeMove;

public class SmartSearch implements Behaviour {

    Character target;

    public SmartSearch(Character character){
        this.target = character;
    }

    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {

        List<RelativeMove> movesToTarget = stage.grid. breadthTo(mover.location,target.location, mover);

        mover.setTargetOf(target.location);

        if (movesToTarget.size() == 0) {
            return new NoMove(stage.grid, mover);
        }
        else
        {

            return movesToTarget.get(0);
        }}


}
