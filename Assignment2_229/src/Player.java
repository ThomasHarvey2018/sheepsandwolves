import bos.GameBoard;

import java.awt.*;

public class Player implements KeyObserver {

    private static Player uniqueInstance;

    public Cell location;
    private Boolean inMove;

    public Player(Cell location){
        this.location = location;
        inMove = false;
    }

    public void paint(Graphics g){
        g.setColor(Color.ORANGE);
        g.fillOval(location.x + location.width / 4, location.y + location.height / 4, location.width / 2, location.height / 2);
    }

    public void setLocationOf(Cell loc){
        this.location = loc;
    }

    public void startMove(){
        inMove = true;
    }

    public Boolean inMove(){
        return inMove;
    }

    public void notify(char c, GameBoard<Cell> gb) {
        if (inMove){
            if (c == 'a') {
                if (gb.leftOf(location).isPresent()) {
                    if (!gb.leftOf(location).get().blocked) {
                        location = gb.leftOf(location).orElse(location);
                        inMove = false;
                    } }
                else inMove = false;
            } else if (c == 'd') {
                if (gb.rightOf(location).isPresent()) {
                    if (!gb.rightOf(location).get().blocked) {
                        location = gb.rightOf(location).orElse(location);
                        inMove = false;
                    }}
                else inMove = false;
            } else if (c == 'w') {
                if ( gb.above(location).isPresent()) {
                    if (!gb.above(location).get().blocked) {
                        location = gb.above(location).orElse(location);
                        inMove = false;
                    } }
                else inMove = false;
            } else if (c == 's') {
                if (gb.below(location).isPresent()) {
                    if (!gb.below(location).get().blocked) {
                        location = gb.below(location).orElse(location);
                        inMove = false;
                    }}
                else inMove = false;
            }

        }
    }

    public Cell getLocationOf() {
        return this.location;
    }
}
