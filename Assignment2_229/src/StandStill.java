import bos.NoMove;
import bos.RelativeMove;

public class StandStill implements Behaviour {
    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        mover.setTargetOf(null);
        return new NoMove(stage.grid, mover);
    }
}
