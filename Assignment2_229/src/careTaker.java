import java.awt.event.KeyEvent;

import bos.GameBoard;
//saves the

public class careTaker implements KeyObserver { //implements KeyObserver
    Memento[] saves;

    public careTaker( ) {
        saves = new Memento[1];
        Memento m =  new Memento(Stage.wolf.getLocationOf(), Stage.sheep.getLocationOf(), Stage.shepherd.getLocationOf(), Stage.player.getLocationOf());
        saves[0] = m;


    }

    void saveMemento () {
        Memento m =  new Memento(Stage.wolf.getLocationOf(), Stage.sheep.getLocationOf(), Stage.shepherd.getLocationOf(), Stage.player.getLocationOf());
        saves[0] = m;
        System.out.println("State saved :)");

    }

    Memento restoreMemento() {
        System.out.println("State restored :)");
        return saves[0];

    }

    @Override
    public void notify(char c, GameBoard<Cell> gb) {
        if (c=='r') {
            Stage.getInstance().restoreSavePoint();
        }
        if (c== KeyEvent.VK_SPACE) {
            Stage.getInstance().createSavePoint();
        }
    }

}
