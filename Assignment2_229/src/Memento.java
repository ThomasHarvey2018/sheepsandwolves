
public class Memento {
    private Cell wolfLocation;
    private Cell sheepLocation;
    private Cell shepLocation;
    private Cell playerLocation;

    public Memento(Cell wl, Cell sl, Cell shl,Cell pl) {

        this.wolfLocation = wl;
        this.sheepLocation = sl;
        this.shepLocation = shl;
        this.playerLocation = pl;

    }
    public Cell getWolfLocation() {
        return wolfLocation;
    }

    public Cell getSheepLocation() {
        return sheepLocation;
    }

    public Cell getShepLocation() {
        return shepLocation;
    }

    public Cell getPlayerLocation() {
        return playerLocation;
    }

}