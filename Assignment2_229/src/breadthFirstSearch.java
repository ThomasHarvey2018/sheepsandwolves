import java.util.ArrayList;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import bos.*;

//code inspired by http://www.peachpit.com/articles/article.aspx?p=101142

public class breadthFirstSearch {

    protected static LinkedList<Cell> constructPath(Cell cell) {
        LinkedList<Cell> path = new LinkedList<Cell>();

        while (cell.parent != null) {
            path.addFirst(cell);
            cell = cell.parent;

        }
        return path;
    }


    public static LinkedList<Cell> makeNeighbours(Cell cell) {
        LinkedList<Cell> path = new LinkedList<Cell>();

        if (Stage.grid.above(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).isPresent())
            path.add(Stage.grid.above(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).get());
        if (Stage.grid.below(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).isPresent())
            path.add(Stage.grid.below(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).get());
        if (Stage.grid.rightOf(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).isPresent())
            path.add(Stage.grid.rightOf(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).get());
        if (Stage.grid.leftOf(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).isPresent())
            path.add(Stage.grid.leftOf(Stage.grid.cellAtRowCol(cell.y/35,cell.x/35)).get());

        cell.neighbours = path;
        return path;

    }


    public static LinkedList<Cell> breadthSearch(Cell from, Cell to) {

        Pair<Integer, Integer> fromIndex = Stage.grid.findAmongstCells((c) -> c == from);
        Pair<Integer, Integer> toIndex = Stage.grid.findAmongstCells((c) -> c == to);

        // list of visited nodes
        LinkedList<Cell> closedList = new LinkedList<Cell>();

        // list of nodes to visit (sorted)
        LinkedList<Cell> openList = new LinkedList<Cell>();
        openList.add(from);
        from.parent = null;

        while (!openList.isEmpty()) {
            Cell cell = openList.removeFirst();
            //System.out.println("x location is"+ (((cell.getLocation().y) -10)/35) + " and y location is " + (((cell.getLocation().x)-10)/35));
            if (cell.equals(to)) {
                // path found!

                break;
            }
            else {
                closedList.add(cell);


                // add neighbors to the open list
                makeNeighbours(cell);
                Iterator<Cell> i = cell.neighbours.iterator();
                while (i.hasNext()) {
                    Cell neighborNode = (Cell) i.next();
                    if (!closedList.contains(neighborNode) &&
                            !openList.contains(neighborNode) && !neighborNode.blocked)
                    {
                        neighborNode.parent = cell;
                        openList.add(neighborNode);
                    }
                }
            }
        }


        LinkedList<Cell> finalPath =  constructPath(to);
        if (!finalPath.isEmpty())
            return finalPath;
        else return null;


    }


}
