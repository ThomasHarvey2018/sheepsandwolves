import java.awt.*;
import java.util.LinkedList;
import java.util.Random;


public class Cell extends Rectangle {

    private static Random rand = new Random();
    Color c;
    public boolean blocked;
    public boolean warps;
    //parent cell for path - source!
    public Cell parent;
    public LinkedList<Cell> neighbours;



    public Cell(int x, int y) {
        super(x, y, 35, 35);
        c = new Color(rand.nextInt(30), rand.nextInt(155)+100, rand.nextInt(30));

        blocked = false;
        warps = false;


    }

    public void paint(Graphics g, Boolean highlighted) {

        if (this.blocked == false && this.warps == false) {
            g.setColor(c);
            g.fillRect(x, y, 35, 35);
            g.setColor(Color.BLACK);
            g.drawRect(x,y, 35, 35);


            if (highlighted) {
                g.setColor(Color.LIGHT_GRAY);
                for(int i = 0; i < 10; i++){
                    g.drawRoundRect(x+1, y+1, 33, 33, i, i);
                }
            }

        }

        else if (this.blocked == true) {
            Color b = new Color(165,42,42);
            g.setColor(Color.BLACK);
            g.fillRect(x, y, 35, 35);
            g.setColor(b);
            for (int i = 7; i <= 35; i+=7) {
                g.drawLine(this.x, this.y+i, this.x+i, this.y);
                g.drawLine(this.x+i, this.y+35, this.x+35, this.y+i);
            }
            //g.drawLine(this.x, this.y+7, this.x+7, this.y);
            //g.drawLine(this.x, this.y+30, this.x+30, this.y);
            //g.drawLine(this.x, this.y+35, this.x+35, this.y);
        }

        else if (this.warps = true) {

            g.setColor(Color.BLACK);
            g.fillOval(x+1, y+1, 33, 33);
            g.setColor(Color.WHITE);
            double xr = x+15;
            int yr = y+5;
            g.fillRect((int) xr,yr,5,25);


        }
    }

    @Override
    public boolean contains(Point target){
        if (target == null)
            return false;
        return super.contains(target);
    }

    public int getGrassHeight(){
        return c.getGreen()/50;
    }
}
