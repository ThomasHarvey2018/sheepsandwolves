How my implementation of the memento pattern maps to the standard memento pattern

I set the stage class as the 'originator' as this is the class that controls the positions and behaviour of the various objects, and 
needs to be able to save itself.

The memento class has instance variables relating to the various aspects the stage wants to save- the locations of the various characters.

The caretaker class creates the new memento object and controls the save and undo methods that are in the stage (originator) class. 